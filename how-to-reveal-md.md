## Create simple presentation with reveal-md

[Installation](#reveal-md-installation)

[Create first markdown](#creating-your-first-reveal-md-markdown-file)

[Running presentation](#loading-markdown-file-with-reveal-md)

[Tips](#tip:-use-escape-key)

---

## reveal-md installation

This is linux OS specific, but should also work on Mac

Create the file ~/.npmrc if you don't already have it, and add the following line to it.

```
prefix=${HOME}/.npm-packages
```

Then install reveal-md as global npm package

```
npm install -g reveal-md
```

---

## Creating your first reveal-md markdown file

Create a new file called `pres1.md` in your favorite editor and copy and paste the following text:


    # Slide 1

    Some text here.

    ---

    ## Slide 2

    More text here.


Save this file.

The takeaway here is that the file is just markdown, but the `---` line is special separator that, when wrapped between two empty lines, creates a new slide.

---

## Configure options file reveal.json

In the same directory as your markdown file, create reveal.json file with following contents:

    {
        "height": "100%",
        "width": "100%",
        "maxScale": 1,
        "history": false,
        "hash": true
    }

These options were needed to make the slide contents stay within frame. See [revealjs.com/config](https://revealjs.com/config/) for more options.

Disabling history fixed issue where internal links, aka non-linear travel, wasn't working with back button.

'hash' allows for the page you're on to be retained during a server reload.

IMPORTANT:  Adjusting the reveal.json requires a full reload of presentation server.  --watch is not enough here.

---

## Loading markdown file with reveal-md

```shell
reveal-md /path/to/pres1.md
```

It's that easy.  Running this command should launch the webpage in your browser.

![](2020-10-11-07-47-34.png)

---

## Slides result

Your newly created presentation should look like this:

![](slides-result-01.gif)

---

## Tip: Use escape key

Press escape once to enter a zoomed out nav

Click on desired slide or arrow key over to desired slide and then press escape key again

![](escape-nav.gif)

More tips below

----

## Tip: Screen capture to gif apps

- Linux
    * Peek - https://github.com/phw/peek

- Mac
    * Gifox - https://gifox.io/

----

## Code highlighter stepping

Add brackets after the language specifier, and denote what lines need highlight for each step.

Code

    ```js [1-2|3|4]
    let a = 1;
    let b = 2;
    let c = x => 1 + 2 + x;
    c(3);
    ```

Result 

```js [1-2|3|4]
let a = 1;
let b = 2;
let c = x => 1 + 2 + x;
c(3);
```

---

## Additional references

reveal-md github https://github.com/webpro/reveal-md

---

## TODOs

* Investigate theming and try to add more of a nav bar, although the zoomed out slide view is very responsive.

* Figure out how to work in a diagram, like draw.io for example.  If it's a static diagram then no worries, just take a screenshot, but need to explore the options here.

---

## end

This is the end

[back to beginning](#/0)