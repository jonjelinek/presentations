# Creating presentations with reveal-md

A simple guide to creating presentations with reveal-md, which is itself a presentation powered by reveal-md.

This is the result of trying to find a good way to show presentations to my peers, in a self-hosted way, that allowed non-linear navigation.

Install

These instructions were written with linux OS in mind, but will be very similar on Mac.

Make sure you have a .npmrc file to avoid needing to use sudo to install global npm packages:

```
cat ~/.npmrc
prefix=${HOME}/.npm-packages
```

Then install reveal-md as global npm package

`npm install -g reveal-md`

Run

`reveal-md how-to-reveal-md.md --watch`